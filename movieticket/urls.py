from django.conf.urls import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'movieapp.views.home', name='home'),
    #url(r'^add/$', 'movieapp.views.add', name='add'),
    # url(r'^movieticket/', include('movieticket.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^login/$', 'movieapp.views.login'),
    url(r'^signup/$', 'movieapp.views.signup'),
    url(r'^chk_login/$', 'movieapp.views.chk_login'),
    url(r'^$', 'movieapp.views.home'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.STATICFILES_DIRS}),

)