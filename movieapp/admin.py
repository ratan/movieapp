from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from movieapp.models import Movie,UserProfile,Ticket,UpcomingShows,Genre,Screen
admin.site.register(UserProfile)
admin.site.register(Movie)
admin.site.register(Ticket)
admin.site.register(UpcomingShows)
admin.site.register(Genre)
admin.site.register(Screen)