# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table('movieapp_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('movieapp', ['UserProfile'])

        # Adding model 'Genre'
        db.create_table('movieapp_genre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('genre', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('movieapp', ['Genre'])

        # Adding model 'Movie'
        db.create_table('movieapp_movie', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cover', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('des', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('cast', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('director', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('release_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('runtime', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('language', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('imdb_url', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('movieapp', ['Movie'])

        # Adding M2M table for field genre on 'Movie'
        db.create_table('movieapp_movie_genre', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('movie', models.ForeignKey(orm['movieapp.movie'], null=False)),
            ('genre', models.ForeignKey(orm['movieapp.genre'], null=False))
        ))
        db.create_unique('movieapp_movie_genre', ['movie_id', 'genre_id'])

        # Adding model 'Screen'
        db.create_table('movieapp_screen', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('movie', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['movieapp.Movie'])),
            ('screen', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('total_seats', self.gf('django.db.models.fields.PositiveIntegerField')(default=100)),
            ('booked_seats', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('movieapp', ['Screen'])

        # Adding model 'Ticket'
        db.create_table('movieapp_ticket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ticket_number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('movie', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['movieapp.Movie'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['movieapp.UserProfile'])),
            ('screen', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['movieapp.Screen'])),
            ('ticket_class', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('ticket_price', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('movieapp', ['Ticket'])

        # Adding model 'UpcomingShows'
        db.create_table('movieapp_upcomingshows', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
        ))
        db.send_create_signal('movieapp', ['UpcomingShows'])

        # Adding M2M table for field movies on 'UpcomingShows'
        db.create_table('movieapp_upcomingshows_movies', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('upcomingshows', models.ForeignKey(orm['movieapp.upcomingshows'], null=False)),
            ('movie', models.ForeignKey(orm['movieapp.movie'], null=False))
        ))
        db.create_unique('movieapp_upcomingshows_movies', ['upcomingshows_id', 'movie_id'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table('movieapp_userprofile')

        # Deleting model 'Genre'
        db.delete_table('movieapp_genre')

        # Deleting model 'Movie'
        db.delete_table('movieapp_movie')

        # Removing M2M table for field genre on 'Movie'
        db.delete_table('movieapp_movie_genre')

        # Deleting model 'Screen'
        db.delete_table('movieapp_screen')

        # Deleting model 'Ticket'
        db.delete_table('movieapp_ticket')

        # Deleting model 'UpcomingShows'
        db.delete_table('movieapp_upcomingshows')

        # Removing M2M table for field movies on 'UpcomingShows'
        db.delete_table('movieapp_upcomingshows_movies')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'movieapp.genre': {
            'Meta': {'object_name': 'Genre'},
            'genre': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'movieapp.movie': {
            'Meta': {'object_name': 'Movie'},
            'cast': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'cover': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'des': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'genre': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['movieapp.Genre']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'language': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'release_date': ('django.db.models.fields.DateTimeField', [], {}),
            'runtime': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'movieapp.screen': {
            'Meta': {'object_name': 'Screen'},
            'booked_seats': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'movie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['movieapp.Movie']"}),
            'screen': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_seats': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'movieapp.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'movie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['movieapp.Movie']"}),
            'screen': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['movieapp.Screen']"}),
            'ticket_class': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ticket_number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ticket_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['movieapp.UserProfile']"})
        },
        'movieapp.upcomingshows': {
            'Meta': {'object_name': 'UpcomingShows'},
            'code': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'movies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['movieapp.Movie']", 'symmetrical': 'False'})
        },
        'movieapp.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['movieapp']