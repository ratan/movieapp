from django.http import HttpResponse, HttpResponseRedirect
from movieapp.models import UserProfile,Movie,Ticket,UpcomingShows,Screen
from datetime import datetime, timedelta
from django.contrib.auth import authenticate
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth import logout
import hashlib, zlib
from django.contrib import auth
import json


def signup(request):
	if request.method == 'GET':
		return render_to_response('signup.html',context_instance=RequestContext(request))
	if request.method == 'POST':
		username = request.POST.get('username')
		email = request.POST.get('email')
		password = hashlib.md5((request.POST.get('password')).encode('utf-8')).hexdigest()
		status = "1"
		date_created = datetime.now()
		user_id = UserProfile.objects.count()
		user_id = user_id + 1
		#import ipdb;ipdb.set_trace()
		user,user_created = UserProfile.objects.get_or_create(user_id = user_id,username = username,email = email,password=password,status = status,date_created= date_created)
		return HttpResponseRedirect("/")

def login(request):
	if request.method=="GET":
		return render_to_response('login.html',context_instance=RequestContext(request))
	if request.method=="POST":
		username = request.POST.get('username', '')
		password = request.POST.get('password', '')
		user = auth.authenticate(username=username, password=password)
		if user is not None and user.is_active:
			auth.login(request, user)

			return HttpResponseRedirect("/")
		else:
			c = RequestContext(request, {
			'invalid': 1
			})
			return render_to_response('login.html',context_instance=c)

def chk_login(request):
	data={}
	if request.user.username:
		#import ipdb;ipdb.set_trace()
		data['status'] = "1"
		movie = Movie.objects.get(title=request.GET.get('movie'))
		screen = Screen.objects.get(movie=movie)
		if (screen.total_seats-screen.booked_seats)>0:
			data['seat_left'] = screen.total_seats-screen.booked_seats
		else:
			data['seat_left'] = "0"

	else:
		data['status'] = "0"
	return HttpResponse(json.dumps(data), mimetype="application/json")



def home(request):
	shows = UpcomingShows.objects.get(code=1)
	c = RequestContext(request, {
		'movies' : shows.movies.all(),
			
	})
	return render_to_response('home.html',context_instance=c)


def movie(request):
	if request.method == 'POST':
		movie_name = request.POST.get('movie_name')
		movie_img = request.POST.get('movie_img')
		movie_des = request.POST.get('movie_des')
		movie_cast = request.POST.get('movie_cast')
		movie_director = request.POST.get('movie_director')
		release_date = request.POST.get('release_date')
		end_date = request.POST.get('end_date')
		movie,movie_created = Movie.objects.get_or_create(movie_name = movie_name,movie_img = movie_img,movie_des = movie_des,movie_cast = movie_cast,movie_director = movie_director,release_date = release_date,end_date = end_date)

def ticket(request):
	if request.method=="POST":
		ticket_number = ""
		movie = Movie.objects.get(movie_name = request.POST.get('movie_name'))
		user_profile = UserProfile.objects.get(user = request.user)
		screen = request.POST.get('screen')
		ticket_class = request.POST.get('ticket_class')
		ticket_price = request.POST.get('ticket_price')


