from django.db import models
from django.contrib.auth.models import User



TICKET_CHOICES = (
    (1,'Gold'),
    (2,'Silver'),
    (3,'Normal')
)


LANGUAGE_CHOICES = (
	(1,'Hindi'),
	(2,'English')
)

GENRE_CHOICES = (
	(0,'None'),
	(1,'Action'),
	(2,'Adventure'),
	(3,'Animation'),
	(4,'Biography'),
	(5,'Comedy'),
	(6,'Crime'),
	(7,'Documentary'),
	(8,'Drama'),
	(9,'Family'),
	(10,'Fantasy'),
	(11,'Film-Noir'),
	(12,'Game-Show'),
	(13,'History'),
	(14,'Horror'),
	(15,'Music'),
	(16,'Musical'),
	(17,'Mystery'),
	(18,'Romance'),
	(19,'Sci-Fi'),
	(20,'Sport'),
	(21,'Thriller'),
	(22,'War')
)



class UserProfile(models.Model):
	user = models.ForeignKey(User)
	username = models.CharField(max_length=10)
	email = models.CharField(max_length=50)
	password = models.CharField(max_length=15)
	status = models.PositiveIntegerField(default=1)
	date_created = models.DateTimeField()

class Genre(models.Model):
	genre = models.PositiveIntegerField(choices = GENRE_CHOICES)
	def __unicode__(self):
		return str(GENRE_CHOICES[self.genre][1])

class Movie(models.Model):
	title = models.CharField(max_length=50)
	genre = models.ManyToManyField(Genre)
	cover = models.CharField(max_length=100)
	des = models.CharField(max_length=500)
	cast = models.CharField(max_length=100)
	director = models.CharField(max_length=20)
	release_date = models.DateTimeField()
	runtime = models.PositiveIntegerField()
	language = models.PositiveIntegerField(choices = LANGUAGE_CHOICES)
	imdb_url = models.CharField(max_length = 200)
	def __unicode__(self):
		return str(self.title)

class Screen(models.Model):
	movie = models.ForeignKey(Movie)
	screen = models.PositiveIntegerField()
	total_seats = models.PositiveIntegerField(default=100)
	booked_seats = models.PositiveIntegerField(default=0)
	def __unicode__(self):
		return str(str(self.screen)+" "+str(self.movie.title))

class Ticket(models.Model):
	ticket_number = models.PositiveIntegerField()
	movie = models.ForeignKey(Movie)
	user = models.ForeignKey(UserProfile)
	screen = models.ForeignKey(Screen)
	ticket_class = models.PositiveIntegerField(choices=TICKET_CHOICES)
	ticket_price = models.PositiveIntegerField()
	def __unicode__(self):
		return str(self.ticket_number)

class UpcomingShows(models.Model):
	code = models.PositiveIntegerField(default=1)
	movies = models.ManyToManyField(Movie)
	def __unicode__(self):
		return str(self.code)



	


