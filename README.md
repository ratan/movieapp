Problem Statement for the App id 

Build a small application which allows the user to buy movie tickets for upcoming shows. The application should support appropriate search and movie booking features. Administrative panel and payment gateway integration is out of the scope of the test.


HOW To setup this app :

step 1 : Install Django and python ( pip install -r requirements.txt )

step 2 : Get the Project Source Code ( git clone git@bitbucket.org:ratan/movieapp.git )

step 3 : SET PATH to the cloned directory

step 4 : python manage.py syncdb ( to create all the database tables)

step 5 : python manage.py runserver (to run the project )






